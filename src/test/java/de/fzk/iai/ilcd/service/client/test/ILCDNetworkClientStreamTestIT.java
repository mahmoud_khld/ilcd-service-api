/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for
 * Applied Computer Science (IAI).
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file. If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/

package de.fzk.iai.ilcd.service.client.test;

import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;

import de.fzk.iai.ilcd.api.app.source.SourceDataSet;
import de.fzk.iai.ilcd.api.binding.generated.source.ReferenceToDigitalFileType;
import de.fzk.iai.ilcd.api.binding.helper.DatasetDAO;
import de.fzk.iai.ilcd.service.client.ILCDServiceClientException;
import de.fzk.iai.ilcd.service.client.impl.FileParam;
import de.fzk.iai.ilcd.service.client.impl.vo.DataStockList;
import de.fzk.iai.ilcd.service.model.ISourceVO;

public class ILCDNetworkClientStreamTestIT extends AbstractILCDNetworkClientTestIT {

	protected final Logger log = org.apache.log4j.Logger.getLogger( this.getClass() );

	private void dumpStream( InputStream stream ) throws IOException {
		BufferedReader in = new BufferedReader( new InputStreamReader( stream ) );
		String line = null;
		while ( (line = in.readLine()) != null ) {
			log.info( line );
		}
	}

	@Test
	public void testGetDataSetsAsStream() throws IOException {
		InputStream is = this.client.getDataSetsAsStream( ISourceVO.class );
		assertTrue( is.available() != 0 );
		dumpStream( is );
	}

	@Test
	public void testGetDataSetVOAsStream() throws IOException, ILCDServiceClientException {
		InputStream is = this.client.getDataSetVOAsStream( ISourceVO.class, SOURCE_UUID );
		assertTrue( is.available() != 0 );
		dumpStream( is );
	}

	@Test
	public void testGetDataSetAsStream() throws IOException, ILCDServiceClientException {
		InputStream is = this.client.getDataSetAsStream( SourceDataSet.class, SOURCE_UUID );
		assertTrue( is.available() != 0 );
		dumpStream( is );
	}

	@Test
	public void testPutDataSetAsStream() throws IOException, ILCDServiceClientException {

		// create a dataset with unique UUID from an existing one
		SourceDataSet ds = this.client.getDataSet( SourceDataSet.class, SOURCE_UUID );

		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat( "ddHHmmss" );
		String origUUID = ds.getSourceInformation().getDataSetInformation().getUUID();
		String newUUID = sdf.format( cal.getTime() ) + origUUID.substring( origUUID.indexOf( "-" ) );

		log.info( "changing UUID to " + newUUID );
		ds.getSourceInformation().getDataSetInformation().setUUID( newUUID );

		// save the ds to disk
		File tmpFile = File.createTempFile( newUUID, ".xml" );
		tmpFile.deleteOnExit();
		String fileName = tmpFile.getAbsolutePath();

		DatasetDAO dao = new DatasetDAO();
		dao.saveDataset( ds, fileName );

		// set up an InputStream from that file
		InputStream fis = new FileInputStream( tmpFile );

		this.client.putDataSetAsStream( SourceDataSet.class, fis );

		InputStream is = this.client.getDataSetAsStream( SourceDataSet.class, newUUID );
		assertTrue( is.available() != 0 );
		dumpStream( is );
	}

	@Test
	public void testPutSourceDataSetAsStream() throws IOException, ILCDServiceClientException {

		// create a dataset with unique UUID from an existing one
		SourceDataSet ds = this.client.getDataSet( SourceDataSet.class, SOURCE_UUID );

		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat( "ddHHmmss" );
		String origUUID = ds.getSourceInformation().getDataSetInformation().getUUID();
		String newUUID = sdf.format( cal.getTime() ) + origUUID.substring( origUUID.indexOf( "-" ) );

		log.info( "changing UUID to " + newUUID );
		ds.getSourceInformation().getDataSetInformation().setUUID( newUUID );

		ReferenceToDigitalFileType ref = new ReferenceToDigitalFileType();
		ref.setUri( "../external_docs/kitt.gif" );
		ds.getSourceInformation().getDataSetInformation().getReferenceToDigitalFile().add( ref );

		// save the ds to disk
		File tmpFile = File.createTempFile( newUUID, ".xml" );
		tmpFile.deleteOnExit();
		String fileName = tmpFile.getAbsolutePath();

		DatasetDAO dao = new DatasetDAO();
		dao.saveDataset( ds, fileName );

		// set up an InputStream from that file
		InputStream fis = new FileInputStream( tmpFile );

		FileParam param = new FileParam( "kit.gif", new FileInputStream( new File( "src/test/other/kitt.gif" ) ) );
		List<FileParam> paramList = new ArrayList<FileParam>();
		paramList.add( param );

		this.client.putSourceDataSetAsStream( fis, null, paramList );

		InputStream is = this.client.getDataSetAsStream( SourceDataSet.class, newUUID );
		assertTrue( is.available() != 0 );
		dumpStream( is );

	}

	@Test
	public void testPutDataSetInStockAsStream() throws IOException, ILCDServiceClientException {

		// create a dataset with unique UUID from an existing one
		SourceDataSet ds = this.client.getDataSet( SourceDataSet.class, SOURCE_UUID );

		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat( "ddHHmmss" );
		String origUUID = ds.getSourceInformation().getDataSetInformation().getUUID();
		String newUUID = sdf.format( cal.getTime() ) + origUUID.substring( origUUID.indexOf( "-" ) );

		log.info( "changing UUID to " + newUUID );
		ds.getSourceInformation().getDataSetInformation().setUUID( newUUID );

		// save the ds to disk
		File tmpFile = File.createTempFile( newUUID, ".xml" );
		tmpFile.deleteOnExit();
		String fileName = tmpFile.getAbsolutePath();

		DatasetDAO dao = new DatasetDAO();
		dao.saveDataset( ds, fileName );

		// set up an InputStream from that file
		InputStream fis = new FileInputStream( tmpFile );

		DataStockList dsList = this.client.getDataStocks();
		
		this.client.putDataSetAsStream( SourceDataSet.class, fis, dsList.getDataStocks().get(0).getUuid() );

		InputStream is = this.client.getDataSetAsStream( SourceDataSet.class, newUUID );
		assertTrue( is.available() != 0 );
		dumpStream( is );

	}

}
