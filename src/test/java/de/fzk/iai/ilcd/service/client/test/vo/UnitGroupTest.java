/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.client.test.vo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.junit.Before;
import org.junit.Test;

import de.fzk.iai.ilcd.service.client.impl.vo.DatasetVODAO;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.ObjectFactory;
import de.fzk.iai.ilcd.service.client.impl.vo.dataset.UnitGroupDataSetVO;
import de.fzk.iai.ilcd.service.model.IUnitGroupVO;

public class UnitGroupTest extends de.fzk.iai.ilcd.service.client.test.vo.VOTest {
	private ObjectFactory of = null;

	@Before
	public void setup() {
		this.of = new ObjectFactory();
	}

	@Test
	public void simpleUnitGroupMarshallingTest() throws Exception {
		UnitGroupDataSetVO dataset = of.unitGroupDataSetVO();
		dataset.setUuid("00000000-0000-0000-0000-000000000000");
		dataset.setName("Foo unit group");
		dataset.setPermanentUri("http://db.ilcd-network.org/data/unitgroups/unitgrouptest");
		dataset.setGeneralComment("foo with the unit group");

		dataset.setDataSetVersion("01.00.000");

		dataset.setDefaultUnit("kg");

		DatasetVODAO dao = new DatasetVODAO();
		dao.setRenderSchemaLocation(true);
		dao.marshal(dataset, System.out);
		dao.marshal(dataset, new FileOutputStream(new File(PATH_PREFIX + "service_api_single_unitgroup_dataset.xml")));

	}

	@Test
	@SuppressWarnings("unused")
	public void simpleUnmarshallingTest() throws Exception {

		FileInputStream stream = new FileInputStream(new File(PATH_PREFIX + "service_api_single_unitgroup_dataset.xml"));

		DatasetVODAO dao = new DatasetVODAO();

		IUnitGroupVO dataset = (IUnitGroupVO) dao.unmarshal(stream);

	}

}
