/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for
 * Applied Computer Science (IAI).
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file. If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/

package de.fzk.iai.ilcd.service.client.test;

import java.net.MalformedURLException;

import javax.ws.rs.core.MultivaluedMap;

import org.apache.log4j.Logger;
import org.junit.Test;

import com.sun.jersey.core.util.MultivaluedMapImpl;

import de.fzk.iai.ilcd.service.client.ILCDServiceClientException;
import de.fzk.iai.ilcd.service.client.impl.ILCDNetworkClient;
import de.fzk.iai.ilcd.service.client.impl.vo.Result;
import de.fzk.iai.ilcd.service.model.IDataSetVO;
import de.fzk.iai.ilcd.service.model.ILCIAMethodVO;
import de.fzk.iai.ilcd.service.model.IProcessVO;

public class QueryTestIT {

	protected final Logger log = org.apache.log4j.Logger.getLogger( this.getClass() );

	@Test
	public void testQuery() throws MalformedURLException, ILCDServiceClientException {

		ILCDNetworkClient conn = new ILCDNetworkClient( ILCDNetworkClientTestIT.getBaseURL() );

		MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
		queryParams.add( "name", "cem" );

		Result<IProcessVO> result = conn.query( IProcessVO.class, queryParams );

		for ( IDataSetVO dataset : result.getDataSets() ) {
			log.info( dataset.getDefaultName() + " " + ((IProcessVO) dataset).getLocation() );
		}

	}

	@Test
	public void testQueryLCIAMethod() throws MalformedURLException, ILCDServiceClientException {

		ILCDNetworkClient conn = new ILCDNetworkClient( ILCDNetworkClientTestIT.getBaseURL() );

		MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
		queryParams.add( "name", "acid" );

		Result<ILCIAMethodVO> result = conn.query( ILCIAMethodVO.class, queryParams );

		for ( IDataSetVO dataset : result.getDataSets() ) {
			log.info( dataset.getDefaultName() + " " + ((ILCIAMethodVO) dataset).getName().getValue() + " "
					+ ((ILCIAMethodVO) dataset).getTimeInformation().getDuration() );
		}

	}
}
