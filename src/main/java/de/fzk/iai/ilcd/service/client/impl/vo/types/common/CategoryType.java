/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for
 * Applied Computer Science (IAI).
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file. If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/

package de.fzk.iai.ilcd.service.client.impl.vo.types.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

import de.fzk.iai.ilcd.service.model.common.IClass;

/**
 * <p>
 * Java class for CategoryType complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CategoryType"&gt;
 *   &lt;simpleContent&gt;
 *     &lt;extension base="&lt;http://lca.jrc.it/ILCD/Common&gt;String"&gt;
 *       &lt;attribute name="level" use="required" type="{http://lca.jrc.it/ILCD/Common}LevelType" /&gt;
 *       &lt;attribute name="catId" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;anyAttribute processContents='lax' namespace='##other'/&gt;
 *     &lt;/extension&gt;
 *   &lt;/simpleContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( name = "CategoryType", propOrder = { "value" } )
public class CategoryType implements IClass {

	@XmlValue
	protected String value;

	@XmlAttribute( required = true )
	protected int level;

	@XmlAttribute
	protected String catId;

	public CategoryType() {
	}

	public CategoryType( int level, String value, String id ) {
		this.level = level;
		this.value = value;
		this.catId = id;
	}

	public CategoryType( int level, String value ) {
		this( level, value, null );
	}

	public boolean equals( IClass cat ) {
		return (this.level == cat.getLevel() && this.getName().equals( cat.getName() ));
	}

	/*
	 * (non-Javadoc)
	 * @see de.fzk.iai.ilcd.api.vo.types.common.IClass#getName()
	 */
	/*
	 * (non-Javadoc)
	 * @see de.fzk.iai.ilcd.api.vo.types.common.ICategory#getName()
	 */
	public String getName() {
		return value;
	}

	/**
	 * Sets the value of the value property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setValue( String value ) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}

	/*
	 * (non-Javadoc)
	 * @see de.fzk.iai.ilcd.api.vo.types.common.IClass#getLevel()
	 */
	/*
	 * (non-Javadoc)
	 * @see de.fzk.iai.ilcd.api.vo.types.common.ICategory#getLevel()
	 */
	public Integer getLevel() {
		return level;
	}

	/**
	 * Sets the value of the level property.
	 * 
	 * @param value
	 *            allowed object is int
	 * 
	 */
	public void setLevel( int value ) {
		this.level = value;
	}

	/**
	 * Gets the value of the catId property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCatId() {
		return catId;
	}

	public String getClId() {
		return getCatId();
	}
	
	/**
	 * Sets the value of the catId property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCatId( String value ) {
		this.catId = value;
	}

}
