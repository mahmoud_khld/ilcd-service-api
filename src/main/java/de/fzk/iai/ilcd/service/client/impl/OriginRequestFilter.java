package de.fzk.iai.ilcd.service.client.impl;

import com.sun.jersey.api.client.ClientRequest;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.filter.ClientFilter;

public class OriginRequestFilter extends ClientFilter {

	private String origin;

	public OriginRequestFilter(String origin) {
		this.origin = origin;
	}

	public ClientResponse handle(ClientRequest request) {

		if (origin != null) {
		  	request.getHeaders().add("X-Origin", origin);
		}

		return getNext().handle(request);
	   
	}
}
