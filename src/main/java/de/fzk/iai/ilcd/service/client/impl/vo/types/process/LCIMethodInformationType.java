/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.client.impl.vo.types.process;

import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import de.fzk.iai.ilcd.service.model.enums.LCIMethodApproachesValue;
import de.fzk.iai.ilcd.service.model.enums.LCIMethodPrincipleValue;
import de.fzk.iai.ilcd.service.model.process.ILCIMethodInformation;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LCIMethodInformationType", propOrder = { "methodPrinciple", "approach" })
public class LCIMethodInformationType implements ILCIMethodInformation {

	@XmlElement(name = "methodPrinciple")
	protected LCIMethodPrincipleValue methodPrinciple;

	@XmlElement(name = "approach")
	protected Set<LCIMethodApproachesValue> approach = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzk.iai.ilcd.api.vo.types.process.ILCIMethodInformation#getMethodPrinciple
	 * ()
	 */
	public LCIMethodPrincipleValue getMethodPrinciple() {
		return methodPrinciple;
	}

	/**
	 * @param lciMethodPrinciple
	 *            the lciMethodPrinciple to set
	 */
	public void setMethodPrinciple(LCIMethodPrincipleValue lciMethodPrinciple) {
		this.methodPrinciple = lciMethodPrinciple;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzk.iai.ilcd.api.vo.types.process.ILCIMethodInformation#getApproaches
	 * ()
	 */
	public Set<LCIMethodApproachesValue> getApproaches() {
		if (approach == null) {
			approach = new HashSet<LCIMethodApproachesValue>();
		}
		return this.approach;
	}
}
