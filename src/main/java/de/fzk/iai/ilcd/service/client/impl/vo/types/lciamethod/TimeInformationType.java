/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.client.impl.vo.types.lciamethod;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import de.fzk.iai.ilcd.service.client.impl.vo.types.common.LString;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.MultiLangString;
import de.fzk.iai.ilcd.service.model.common.IMultiLangString;
import de.fzk.iai.ilcd.service.model.lciamethod.ITimeInformation;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TimeType", propOrder = { "referenceYear", "duration" })
public class TimeInformationType implements ITimeInformation {

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/LCIAMethod", type = LString.class)
	protected List<LString> referenceYear;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/LCIAMethod", type = LString.class)
	protected List<LString> duration;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzk.iai.ilcd.api.vo.types.lciamethod.ITimeInformation#getReferenceYear
	 * ()
	 */
	public IMultiLangString getReferenceYear() {
		if (referenceYear == null) {
			referenceYear = new ArrayList<LString>();
		}
		return new MultiLangString(referenceYear);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.types.lciamethod.ITimeInformation#getDuration
	 * ()
	 */
	public IMultiLangString getDuration() {
		if (duration == null) {
			duration = new ArrayList<LString>();
		}
		return new MultiLangString(duration);
	}

}
