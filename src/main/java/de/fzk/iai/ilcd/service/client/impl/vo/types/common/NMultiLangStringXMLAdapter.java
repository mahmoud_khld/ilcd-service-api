package de.fzk.iai.ilcd.service.client.impl.vo.types.common;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.apache.log4j.Logger;

public class NMultiLangStringXMLAdapter extends XmlAdapter<NMultiLangString, MultiLangString> {

	private final Logger log = org.apache.log4j.Logger.getLogger( this.getClass() );

	private String lang;

	public NMultiLangStringXMLAdapter( String lang ) {
		if ( log.isDebugEnabled() )
			log.debug( "init " + lang );
		this.lang = lang;
	}

	@Override
	public MultiLangString unmarshal( NMultiLangString v ) throws Exception {
		if ( log.isDebugEnabled() )
			log.debug( "unmarshal" );
		MultiLangString m = (MultiLangString) v;
		return m;
	}

	@Override
	public NMultiLangString marshal( MultiLangString v ) throws Exception {
		if ( log.isDebugEnabled() )
			log.debug( "marshal" );
		NMultiLangString n = new NMultiLangString( v );
		n.setDefaultLanguage( lang );
		return n;
	}

}
