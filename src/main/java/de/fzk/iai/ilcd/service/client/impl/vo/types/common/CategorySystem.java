package de.fzk.iai.ilcd.service.client.impl.vo.types.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import de.fzk.iai.ilcd.service.model.ICategorySystem;

@XmlAccessorType( XmlAccessType.FIELD )
@XmlType( name = "CategorySystem" )
public class CategorySystem implements ICategorySystem {

	@XmlAttribute( required = true )
	protected String name;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI")
	protected GlobalReferenceType referenceToSource;

	public CategorySystem() {
		
	}

	public String getName() {
		return name;
	}

	public void setName( String name ) {
		this.name = name;
	}

	public GlobalReferenceType getReferenceToSource() {
		return referenceToSource;
	}

	public void setReferenceToSource(GlobalReferenceType reference) {
		this.referenceToSource = reference;
	}

}
