/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.model.common;

import java.util.List;

/**
 * This interface represents a string field that can consist of multiple
 * languages. It specifies a default language.
 */
public interface IMultiLangString {

	/** The default language */
	public static final String DEFAULT_LANGUAGE = "en";

	/**
	 * Gets the value for the default language.
	 * 
	 * This doesn't necessarily need to be the one declared for
	 * DEFAULT_LANGUAGE, but may be implementation-specific.
	 * 
	 * @return the value
	 */
	public abstract String getValue();
	public abstract String getDefaultValue();

	/**
	 * Gets the value for the given language id.
	 * 
	 * @param lang
	 *            the lang
	 * @return the value
	 */
	public abstract String getValue(String lang);

	/**
	 * Sets the value for the default language.
	 * 
	 * @param value
	 *            the new value
	 */
	public abstract void setValue(String value);

	/**
	 * Sets the value for the given language id.
	 * 
	 * @param lang
	 *            the lang
	 * @param value
	 *            the value
	 */
	public abstract void setValue(String lang, String value);

	/**
	 * Gets the internal list of ILString items.
	 * 
	 * @return the list of ILString items
	 */
	public List<ILString> getLStrings();

	public abstract String getValueWithFallback(String lang);
}
