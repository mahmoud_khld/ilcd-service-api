package de.fzk.iai.ilcd.service.model;

import java.util.List;


public interface IIntegerList extends IList {
	
	public List<Integer> getIntegers();

}
