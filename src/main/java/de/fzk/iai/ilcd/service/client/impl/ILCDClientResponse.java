/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.client.impl;

import com.sun.jersey.api.client.ClientResponse;


public class ILCDClientResponse {

	public ILCDClientResponse(ClientResponse cr) {
		this.body = cr.getEntity(String.class);
		this.clientResponse = cr;
		this.status = cr.getStatus();
		
	}
	
	protected int status;
	
	protected ClientResponse clientResponse;
	
	protected String body;

	protected String message;

	/**
	 * @deprecated
	 * @return the body
	 */
	public String getBody() {
		return body;
	}

	/**
	 * @deprecated
	 * @param body the body to set
	 */
	public void setBody(String body) {
		this.body = body;
	}

	/**
	 * @return the clientResponse
	 */
	public ClientResponse getClientResponse() {
		return clientResponse;
	}

	/**
	 * @param clientResponse the clientResponse to set
	 */
	public void setClientResponse(ClientResponse clientResponse) {
		this.clientResponse = clientResponse;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	
}
