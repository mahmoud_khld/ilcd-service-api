/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/

package de.fzk.iai.ilcd.service.client.impl.vo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import de.fzk.iai.ilcd.service.model.IDataStockListVO;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "dataStock" })
@XmlRootElement(name = "dataStockList", namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI")
public class DataStockList implements IDataStockListVO {

	@XmlAnyElement
	@XmlElementRef
	protected List<DataStockVO> dataStock = null;

	/**
	 * @return the dataSetList
	 */
	public List<DataStockVO> getDataStocks() {
		if (this.dataStock == null)
			this.dataStock = new ArrayList<DataStockVO>();
		return dataStock;
	}

	/**
	 * @param dataSet
	 *            the dataSetList to set
	 */
	public void setDataSet(List<DataStockVO> dataSet) {
		this.dataStock = dataSet;
	}

}
