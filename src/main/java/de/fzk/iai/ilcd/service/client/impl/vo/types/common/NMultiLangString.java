package de.fzk.iai.ilcd.service.client.impl.vo.types.common;

import de.fzk.iai.ilcd.service.model.common.IMultiLangString;

public class NMultiLangString extends MultiLangString implements IMultiLangString {

	public NMultiLangString(MultiLangString m) {
		this.lStrings = m.lStrings;
	}
	
	private String defaultLanguage;

	public String getDefaultLanguage() {
		return defaultLanguage;
	}

	public void setDefaultLanguage( String defaultLanguage ) {
		this.defaultLanguage = defaultLanguage;
	}

	@Override
	public String getValue() {
		return getValue( this.defaultLanguage );
	}

}
