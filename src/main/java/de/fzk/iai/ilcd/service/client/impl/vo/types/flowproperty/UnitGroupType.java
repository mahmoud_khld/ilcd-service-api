/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.client.impl.vo.types.flowproperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import de.fzk.iai.ilcd.service.client.impl.vo.types.common.GlobalReferenceType;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.LString;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.MultiLangString;
import de.fzk.iai.ilcd.service.model.common.IGlobalReference;
import de.fzk.iai.ilcd.service.model.common.IMultiLangString;
import de.fzk.iai.ilcd.service.model.flowproperty.IUnitGroupType;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UnitGroupType", propOrder = { "name", "defaultUnit", "reference" })
public class UnitGroupType implements IUnitGroupType {

    @XmlElement(type = LString.class)
	protected List<LString> name = new ArrayList<LString>();

	protected String defaultUnit;

	@XmlAttribute(namespace = "http://www.w3.org/1999/xlink")
	@XmlSchemaType(name = "anyURI")
	protected String href;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI")
	protected GlobalReferenceType reference;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.types.flowproperty.IUnitGroupType#getName()
	 */
	public IMultiLangString getName() {
		if (name == null) {
			name = new ArrayList<LString>();
		}
		return new MultiLangString(name);
	}

	protected void setName(List<LString> name) {
		this.name = name;
	}

	public void setName(LString name) {
		if (this.name == null) {
			this.name = new ArrayList<LString>();
		}
		name.insertWithoutDuplicates(this.name);
	}

	public void setName(String lang, String value) {
		LString lString = new LString(lang, value);
		this.setName(lString);
	}

	public void setName(String purpose) {
		LString lString = new LString(purpose);
		this.setName(lString);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzk.iai.ilcd.api.vo.types.flowproperty.IUnitGroupType#getDefaultUnit()
	 */
	public String getDefaultUnit() {
		return defaultUnit;
	}

	/**
	 * @param defaultUnit
	 *            the defaultUnit to set
	 */
	public void setDefaultUnit(String defaultUnit) {
		this.defaultUnit = defaultUnit;
	}

	/**
	 * @return the href
	 */
	public String getHref() {
		return href;
	}

	/**
	 * @param href
	 *            the href to set
	 */
	public void setHref(String href) {
		this.href = href;
	}

	/**
	 * @return the reference
	 */
	public IGlobalReference getReference() {
		return reference;
	}

	/**
	 * @param referenceToFlowProperty
	 *            the referenceToFlowProperty to set
	 */
	public void setReference(IGlobalReference referenceToFlowProperty) {
		this.reference = (GlobalReferenceType) referenceToFlowProperty;
	}

}
