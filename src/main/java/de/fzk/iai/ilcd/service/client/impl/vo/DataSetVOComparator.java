/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.client.impl.vo;

import java.util.Comparator;

import org.apache.commons.collections.comparators.NullComparator;
import org.apache.log4j.Logger;

import de.fzk.iai.ilcd.service.client.impl.vo.dataset.DataSetVO;
import de.fzk.iai.ilcd.service.model.IProcessVO;

public class DataSetVOComparator implements Comparator<DataSetVO> {

	protected static Logger log = Logger.getLogger(DataSetVOComparator.class);

	private NullComparator nullComp = new NullComparator(false);

	public int compare(DataSetVO o1, DataSetVO o2) {
		int result;

		result = nullComp.compare(o1.getTopCategory(), o2.getTopCategory());
		if (result != 0)
			return result;

		result = nullComp.compare(o1.getSubCategory(), o2.getSubCategory());
		if (result != 0)
			return result;

		result = nullComp.compare(o1.getDefaultName(), o2.getDefaultName());

		if (result != 0)
			return result;

		if (o1 instanceof IProcessVO && o2 instanceof IProcessVO) {
			result = nullComp.compare(((IProcessVO) o1).getLocation(), ((IProcessVO) o2).getLocation());
			if (result != 0)
				return result;
		}

		return 1;

	};

}
