/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.client.impl.vo.dataset;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import de.fzk.iai.ilcd.service.client.impl.vo.types.common.LString;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.MultiLangString;
import de.fzk.iai.ilcd.service.client.impl.vo.types.flow.FlowCategorizationType;
import de.fzk.iai.ilcd.service.client.impl.vo.types.flow.ReferenceFlowPropertyType;
import de.fzk.iai.ilcd.service.model.IFlowVO;
import de.fzk.iai.ilcd.service.model.common.IMultiLangString;
import de.fzk.iai.ilcd.service.model.enums.TypeOfFlowValue;

@XmlRootElement(name = "flow", namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Flow")
@XmlType(propOrder = { "synonyms", "flowCategorization", "type", "casNumber", "sumFormula", "referenceFlowProperty", "locationOfSupply" })
public class FlowDataSetVO extends DataSetVO implements IFlowVO {

	public FlowDataSetVO() {
	}

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Flow")
	protected TypeOfFlowValue type;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Flow")
	protected FlowCategorizationType flowCategorization;

	@XmlElement(type = LString.class, namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI")
	protected List<LString> synonyms = new ArrayList<LString>();

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Flow")
	protected String casNumber;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Flow")
	protected String sumFormula;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Flow")
	protected ReferenceFlowPropertyType referenceFlowProperty;

	@XmlElement(type = LString.class, namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Flow")
	protected List<LString> locationOfSupply = new ArrayList<LString>();


	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IFlowVO#getType()
	 */
	public TypeOfFlowValue getType() {
		return type;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IFlowVO#setType(de.fzk.iai.ilcd
	 * .api.vo.types.flow.TypeOfFlowValue)
	 */
	public void setType(TypeOfFlowValue type) {
		this.type = type;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IFlowVO#getFlowCategorization()
	 */
	public FlowCategorizationType getFlowCategorization() {
		if (this.flowCategorization == null)
			this.flowCategorization = new FlowCategorizationType();
		return flowCategorization;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IFlowVO#setFlowCategorization(de
	 * .fzk.iai.ilcd.api.vo.types.flow.FlowCategorizationType)
	 */
	public void setFlowCategorization(FlowCategorizationType flowCategorization) {
		this.flowCategorization = flowCategorization;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IFlowVO#getSynonyms()
	 */
	public IMultiLangString getSynonyms() {
		if (synonyms == null) {
			synonyms = new ArrayList<LString>();
		}
		return new MultiLangString(synonyms);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IFlowVO#getCasNumber()
	 */
	public String getCasNumber() {
		return casNumber;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IFlowVO#setCasNumber(java.lang.
	 * String)
	 */
	public void setCasNumber(String casNumber) {
		this.casNumber = casNumber;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IFlowVO#getSumFormula()
	 */
	public String getSumFormula() {
		return sumFormula;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IFlowVO#setSumFormula(java.lang
	 * .String)
	 */
	public void setSumFormula(String sumFormula) {
		this.sumFormula = sumFormula;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IFlowVO#getReferenceFlowProperty()
	 */
	public ReferenceFlowPropertyType getReferenceFlowProperty() {
		return referenceFlowProperty;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IFlowVO#setReferenceFlowProperty
	 * (de.fzk.iai.ilcd.api.vo.types.flow.ReferenceFlowPropertyType)
	 */
	public void setReferenceFlowProperty(ReferenceFlowPropertyType referenceFlowProperty) {
		this.referenceFlowProperty = referenceFlowProperty;
	}

	public IMultiLangString getLocationOfSupply() {
		if (locationOfSupply == null) {
			locationOfSupply = new ArrayList<LString>();
		}
		return new MultiLangString(locationOfSupply);
	}

}
