package de.fzk.iai.ilcd.service.model;

import java.util.List;


public interface IStringList extends IList {
	
	public List<String> getStrings();

}
