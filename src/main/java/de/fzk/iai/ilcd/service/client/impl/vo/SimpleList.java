package de.fzk.iai.ilcd.service.client.impl.vo;

import javax.xml.bind.annotation.XmlTransient;

public abstract class SimpleList {

	private String identifier = null;

	public SimpleList() {
		super();
	}

	@XmlTransient
	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

}