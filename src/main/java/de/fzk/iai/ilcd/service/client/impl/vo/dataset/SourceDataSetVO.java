/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.client.impl.vo.dataset;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import de.fzk.iai.ilcd.service.client.impl.vo.types.common.GlobalReferenceType;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.LString;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.MultiLangString;
import de.fzk.iai.ilcd.service.model.ISourceVO;
import de.fzk.iai.ilcd.service.model.common.IGlobalReference;
import de.fzk.iai.ilcd.service.model.enums.PublicationTypeValue;

@XmlRootElement(name = "source", namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Source")
@XmlType(propOrder = { "citation", "publicationType", "files", "belongsTo" })
public class SourceDataSetVO extends DataSetVO implements ISourceVO {

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Source", type = LString.class)
	protected List<LString> citation;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Source")
	protected PublicationTypeValue publicationType;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Source", name = "file")
	protected List<GlobalReferenceType> files;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Source")
	protected List<GlobalReferenceType> belongsTo;

	public SourceDataSetVO() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.ISourceVO#getCitation()
	 */
	public MultiLangString getCitation() {
		if (citation == null)
			citation = new ArrayList<LString>();
		return new MultiLangString(citation);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzk.iai.ilcd.api.vo.dataset.ISourceVO#setCitation(java.lang.String)
	 */
	public void addCitation(LString citation) {
		if (this.citation == null)
			this.citation = new ArrayList<LString>();
		// inserts or updates LString within list
		citation.insertWithoutDuplicates(this.citation);
	}

	public void setCitation(String lang, String value) {
		LString lString = new LString(lang, value);
		this.addCitation(lString);
	}

	public void setCitation(String value) {
		LString lString = new LString(value);
		this.addCitation(lString);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.ISourceVO#getPublicationType()
	 */
	public PublicationTypeValue getPublicationType() {
		return publicationType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzk.iai.ilcd.api.vo.dataset.ISourceVO#setPublicationType(de.fzk.iai
	 * .ilcd.api.vo.types.source.PublicationTypeValue)
	 */
	public void setPublicationType(PublicationTypeValue publicationType) {
		this.publicationType = publicationType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.ISourceVO#getFile()
	 */
	public List<GlobalReferenceType> getFiles() {
		return files;
	}

	public List<IGlobalReference> getFileReferences() {
		List<IGlobalReference> fileRefs = new ArrayList<IGlobalReference>();
		if (files == null)
			files = new ArrayList<GlobalReferenceType>();
		for (GlobalReferenceType refType : files)
			fileRefs.add(refType);
		return fileRefs;
	}

	public void addFileReference(GlobalReferenceType fileRef) {
		if (files == null)
			files = new ArrayList<GlobalReferenceType>();
		files.add(fileRef);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzk.iai.ilcd.api.vo.dataset.ISourceVO#setFile(de.fzk.iai.ilcd.api.
	 * vo.types.common.GlobalReferenceType)
	 */
	protected void setFiles(List<GlobalReferenceType> fileRefs) {
		this.files = fileRefs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.ISourceVO#getBelongsTo()
	 */
	public List<IGlobalReference> getBelongsTo() {
		if (belongsTo == null) {
			belongsTo = new ArrayList<GlobalReferenceType>();
		}
		List<IGlobalReference> belongs = new ArrayList<IGlobalReference>();
		for (GlobalReferenceType ref : belongsTo) {
			belongs.add(ref);
		}
		return belongs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzk.iai.ilcd.api.vo.dataset.ISourceVO#setBelongsTo(de.fzk.iai.ilcd
	 * .api.vo.types.common.GlobalReferenceType)
	 */
	protected void setBelongsTo(List<GlobalReferenceType> belongsTo) {
		this.belongsTo = belongsTo;
	}

	public void addBelongsTo(GlobalReferenceType belongsTo) {
		if (this.belongsTo == null)
			this.belongsTo = new ArrayList<GlobalReferenceType>();
		this.belongsTo.add(belongsTo);
	}

}
