/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.client.impl.vo.dataset;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import de.fzk.iai.ilcd.service.client.impl.vo.types.common.LString;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.MultiLangString;
import de.fzk.iai.ilcd.service.client.impl.vo.types.flowproperty.UnitGroupType;
import de.fzk.iai.ilcd.service.model.IFlowPropertyVO;
import de.fzk.iai.ilcd.service.model.common.IMultiLangString;

@XmlRootElement(name = "flowProperty", namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/FlowProperty")
@XmlType(propOrder = { "synonyms", "unitGroupDetails" })
public class FlowPropertyDataSetVO extends DataSetVO implements IFlowPropertyVO {

	@XmlElement(type = LString.class, namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI")
	protected List<LString> synonyms = new ArrayList<LString>();

	@XmlElement(name="unitGroup", namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/FlowProperty")
	protected UnitGroupType unitGroupDetails;

	public FlowPropertyDataSetVO() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IFlowPropertyVO#getSynonyms()
	 */
	public IMultiLangString getSynonyms() {
		if (synonyms == null) {
			synonyms = new ArrayList<LString>();
		}
		return new MultiLangString(synonyms);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.IFlowPropertyVO#getUnitGroup()
	 */
	public UnitGroupType getUnitGroupDetails() {
		return unitGroupDetails;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzk.iai.ilcd.api.vo.dataset.IFlowPropertyVO#setUnitGroup(de.fzk.iai
	 * .ilcd.api.vo.types.flowproperty.UnitGroupType)
	 */
	public void setUnitGroupDetails(UnitGroupType unitGroupDetails) {
		this.unitGroupDetails = unitGroupDetails;
	}

}
