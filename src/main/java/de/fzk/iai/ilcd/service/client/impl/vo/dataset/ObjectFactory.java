/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.client.impl.vo.dataset;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

@XmlRegistry
public class ObjectFactory {
	private final static QName _ProcessDataSet_QNAME = new QName("http://www.ilcd-network.org/ILCD/ServiceAPI/Process", "processDataSet");

	private final static QName _FlowDataSet_QNAME = new QName("http://www.ilcd-network.org/ILCD/ServiceAPI/Flow", "flowDataSet");

	private final static QName _FlowPropertyDataSet_QNAME = new QName("http://www.ilcd-network.org/ILCD/ServiceAPI/FlowProperty", "flowPropertyDataSet");

	private final static QName _UnitGroupDataSet_QNAME = new QName("http://www.ilcd-network.org/ILCD/ServiceAPI/UnitGroup", "unitGroupDataSet");

	private final static QName _LCIAMethodDataSet_QNAME = new QName("http://www.ilcd-network.org/ILCD/ServiceAPI/LCIAMethod", "LCIAMethodDataSet");

	private final static QName _SourceDataSet_QNAME = new QName("http://www.ilcd-network.org/ILCD/ServiceAPI/Source", "sourceDataSet");

	private final static QName _ContactDataSet_QNAME = new QName("http://www.ilcd-network.org/ILCD/ServiceAPI/Contact", "contactDataSet");

	private final static QName _DataSetList_QNAME = new QName("http://www.ilcd-network.org/ILCD/ServiceAPI", "dataSetList");

	public ProcessDataSetVO processDataSetVO() {
		return new ProcessDataSetVO();
	}

	public FlowDataSetVO flowDataSetVO() {
		return new FlowDataSetVO();
	}

	public FlowPropertyDataSetVO flowPropertyDataSetVO() {
		return new FlowPropertyDataSetVO();
	}

	public UnitGroupDataSetVO unitGroupDataSetVO() {
		return new UnitGroupDataSetVO();
	}

	public LCIAMethodDataSetVO LCIAMethodDataSetVO() {
		return new LCIAMethodDataSetVO();
	}

	public ContactDataSetVO contactDataSetVO() {
		return new ContactDataSetVO();
	}

	public SourceDataSetVO sourceDataSetVO() {
		return new SourceDataSetVO();
	}

	public DataSetList dataSetList() {
		return new DataSetList();
	}

	@XmlElementDecl(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Process", name = "process")
	public JAXBElement<ProcessDataSetVO> createProcessDataSetVO(ProcessDataSetVO value) {
		return new JAXBElement<ProcessDataSetVO>(_ProcessDataSet_QNAME, ProcessDataSetVO.class, null, value);
	}

	@XmlElementDecl(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Flow", name = "flow")
	public JAXBElement<FlowDataSetVO> createFlowDataSetVO(FlowDataSetVO value) {
		return new JAXBElement<FlowDataSetVO>(_FlowDataSet_QNAME, FlowDataSetVO.class, null, value);
	}

	@XmlElementDecl(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/FlowProperty", name = "flowProperty")
	public JAXBElement<FlowPropertyDataSetVO> createFlowPropertyDataSetVO(FlowPropertyDataSetVO value) {
		return new JAXBElement<FlowPropertyDataSetVO>(_FlowPropertyDataSet_QNAME, FlowPropertyDataSetVO.class, null, value);
	}

	@XmlElementDecl(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/UnitGroup", name = "unitGroup")
	public JAXBElement<UnitGroupDataSetVO> createUnitGroupDataSetVO(UnitGroupDataSetVO value) {
		return new JAXBElement<UnitGroupDataSetVO>(_UnitGroupDataSet_QNAME, UnitGroupDataSetVO.class, null, value);
	}

	@XmlElementDecl(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/LCIAMethod", name = "LCIAMethod")
	public JAXBElement<LCIAMethodDataSetVO> createLCIAMethodDataSetVO(LCIAMethodDataSetVO value) {
		return new JAXBElement<LCIAMethodDataSetVO>(_LCIAMethodDataSet_QNAME, LCIAMethodDataSetVO.class, null, value);
	}

	@XmlElementDecl(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Source", name = "source")
	public JAXBElement<SourceDataSetVO> createSourceDataSetVO(SourceDataSetVO value) {
		return new JAXBElement<SourceDataSetVO>(_SourceDataSet_QNAME, SourceDataSetVO.class, null, value);
	}

	@XmlElementDecl(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Contact", name = "contact")
	public JAXBElement<ContactDataSetVO> createContactDataSetVO(ContactDataSetVO value) {
		return new JAXBElement<ContactDataSetVO>(_ContactDataSet_QNAME, ContactDataSetVO.class, null, value);
	}

	@XmlElementDecl(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI", name = "dataSetList")
	public JAXBElement<DataSetList> createDataSetList(DataSetList value) {
		return new JAXBElement<DataSetList>(_DataSetList_QNAME, DataSetList.class, null, value);
	}
}
