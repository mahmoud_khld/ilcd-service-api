package de.fzk.iai.ilcd.service.client.impl.vo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import de.fzk.iai.ilcd.service.client.impl.vo.types.common.CategorySystem;
import de.fzk.iai.ilcd.service.model.ICategorySystem;
import de.fzk.iai.ilcd.service.model.ICategorySystemList;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "categorySystems" })
@XmlRootElement(name = "categorySystems", namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI")
public class CategorySystemList implements ICategorySystemList {

	@XmlElement(name = "categorySystem", type = CategorySystem.class)
	protected List<ICategorySystem> categorySystems;

	public CategorySystemList() {
	}
	
	public List<ICategorySystem> getCategorySystems() {
		if (categorySystems == null)
			categorySystems = new ArrayList<ICategorySystem>();
		return categorySystems;
	}

	public void setCategories(List<ICategorySystem> categorySystem) {
		this.categorySystems = categorySystem;
	}

}
