/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.client.impl.vo.types.process;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.namespace.QName;

import de.fzk.iai.ilcd.service.client.impl.vo.types.common.GlobalReferenceType;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.LString;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.MultiLangString;
import de.fzk.iai.ilcd.service.model.common.IGlobalReference;
import de.fzk.iai.ilcd.service.model.common.IMultiLangString;
import de.fzk.iai.ilcd.service.model.enums.MethodOfReviewValue;
import de.fzk.iai.ilcd.service.model.enums.ScopeOfReviewValue;
import de.fzk.iai.ilcd.service.model.enums.TypeOfReviewValue;
import de.fzk.iai.ilcd.service.model.process.IDataQualityIndicator;
import de.fzk.iai.ilcd.service.model.process.IReview;
import de.fzk.iai.ilcd.service.model.process.IScope;

/**
 * <p>
 * Java class for ReviewType complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType name="ReviewType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;group ref="{http://lca.jrc.it/ILCD/Common}ValidationGroup1"/&gt;
 *         &lt;group ref="{http://lca.jrc.it/ILCD/Common}ValidationGroup3"/&gt;
 *         &lt;element ref="{http://lca.jrc.it/ILCD/Common}other" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="type" type="{http://lca.jrc.it/ILCD/Common}TypeOfReviewValue" /&gt;
 *       &lt;anyAttribute processContents='lax' namespace='##other'/&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReviewType", propOrder = { "scopes", "dataQualityIndicators", "reviewDetails", "referencesToReviewers", "otherReviewDetails",
		"referenceToReviewReport" })
public class ReviewType implements IReview {

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Process", name = "scope", type = ReviewType.Scope.class)
	protected Set<IScope> scopes = null;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Process")
	protected DataQualityIndicatorsType dataQualityIndicators = null;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Process", type = LString.class)
	protected List<LString> reviewDetails = null;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Process", name = "reviewer", type = GlobalReferenceType.class)
	protected List<GlobalReferenceType> referencesToReviewers = null;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Process", type = LString.class)
	protected List<LString> otherReviewDetails = null;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Process", name = "report")
	protected GlobalReferenceType referenceToReviewReport = null;

	@XmlAttribute
	protected TypeOfReviewValue type = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.types.process.IReview#getScopes()
	 */
	public Set<IScope> getScopes() {
		if (scopes == null) {
			scopes = new HashSet<IScope>();
		}
		return this.scopes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fzk.iai.ilcd.api.vo.types.process.IReview#getDataQualityIndicators()
	 */
	public Set<IDataQualityIndicator> getDataQualityIndicators() {
		if (dataQualityIndicators == null)
			return null;
		return dataQualityIndicators.getDataQualityIndicator();
	}

	/**
	 * Sets the value of the dataQualityIndicators property.
	 * 
	 * @param value
	 *            allowed object is {@link DataQualityIndicatorsType }
	 * 
	 */
	public void setDataQualityIndicators(DataQualityIndicatorsType value) {
		this.dataQualityIndicators = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.types.process.IReview#getReviewDetails()
	 */
	public IMultiLangString getReviewDetails() {
		if (reviewDetails == null) {
			reviewDetails = new ArrayList<LString>();
		}
		return new MultiLangString(reviewDetails);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.fzk.iai.ilcd.api.vo.types.process.IReview#
	 * getReferenceToNameOfReviewerAndInstitution()
	 */
	@SuppressWarnings("unchecked")
	public List<IGlobalReference> getReferencesToReviewers() {
		if (referencesToReviewers == null) {
			referencesToReviewers = new ArrayList<GlobalReferenceType>();
		}
		return (List<IGlobalReference>) (List) this.referencesToReviewers;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.types.process.IReview#getOtherReviewDetails()
	 */
	public IMultiLangString getOtherReviewDetails() {
		if (otherReviewDetails == null) {
			otherReviewDetails = new ArrayList<LString>();
		}
		return new MultiLangString(otherReviewDetails);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seede.fzk.iai.ilcd.api.vo.types.process.IReview#
	 * getReferenceToCompleteReviewReport()
	 */
	public GlobalReferenceType getReferenceToReviewReport() {
		return referenceToReviewReport;
	}

	/**
	 * Sets the value of the referenceToCompleteReviewReport property.
	 * 
	 * @param value
	 *            allowed object is {@link GlobalReferenceType }
	 * 
	 */
	public void setReferenceToCompleteReviewReport(GlobalReferenceType value) {
		this.referenceToReviewReport = (GlobalReferenceType) value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.types.process.IReview#getType()
	 */
	public TypeOfReviewValue getType() {
		return type;
	}

	/**
	 * Sets the value of the type property.
	 * 
	 * @param value
	 *            allowed object is {@link TypeOfReviewValue }
	 * 
	 */
	public void setType(TypeOfReviewValue value) {
		this.type = value;
	}

	/**
	 * <p>
	 * Java class for anonymous complex type.
	 * 
	 * <p>
	 * The following schema fragment specifies the expected content contained
	 * within this class.
	 * 
	 * <pre>
	 * &lt;complexType&gt;
	 *   &lt;complexContent&gt;
	 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
	 *       &lt;sequence&gt;
	 *         &lt;element name="method" maxOccurs="unbounded" minOccurs="0"&gt;
	 *           &lt;complexType&gt;
	 *             &lt;complexContent&gt;
	 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
	 *                 &lt;attribute name="name" use="required" type="{http://lca.jrc.it/ILCD/Common}MethodOfReviewValue" /&gt;
	 *                 &lt;anyAttribute processContents='lax' namespace='##other'/&gt;
	 *               &lt;/restriction&gt;
	 *             &lt;/complexContent&gt;
	 *           &lt;/complexType&gt;
	 *         &lt;/element&gt;
	 *       &lt;/sequence&gt;
	 *       &lt;attribute name="name" use="required" type="{http://lca.jrc.it/ILCD/Common}ScopeOfReviewValue" /&gt;
	 *       &lt;anyAttribute processContents='lax' namespace='##other'/&gt;
	 *     &lt;/restriction&gt;
	 *   &lt;/complexContent&gt;
	 * &lt;/complexType&gt;
	 * </pre>
	 * 
	 * 
	 */
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(propOrder = { "method" })
	public static class Scope implements IScope {

		@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Process", type = ReviewType.Scope.Method.class)
		protected List<Method> method;

		@XmlAttribute(required = true)
		protected ScopeOfReviewValue name;

		@XmlAnyAttribute
		private Map<QName, String> otherAttributes = new HashMap<QName, String>();

		/*
		 * (non-Javadoc)
		 * 
		 * @see de.fzk.iai.ilcd.api.vo.types.process.IScope#getMethod()
		 */
		public List<Method> getMethod() {
			if (method == null) {
				method = new ArrayList<Method>();
			}
			return this.method;
		}

		public EnumSet<MethodOfReviewValue> getMethods() {
			EnumSet<MethodOfReviewValue> methodNames = EnumSet.noneOf(MethodOfReviewValue.class);
			for (Method singleMethod : method) {
				methodNames.add(singleMethod.getName());
			}

			return methodNames;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see de.fzk.iai.ilcd.api.vo.types.process.IScope#getName()
		 */
		public ScopeOfReviewValue getName() {
			return name;
		}

		/**
		 * Sets the value of the name property.
		 * 
		 * @param value
		 *            allowed object is {@link ScopeOfReviewValue }
		 * 
		 */
		public void setName(ScopeOfReviewValue value) {
			this.name = value;
		}

		/**
		 * Gets a map that contains attributes that aren't bound to any typed
		 * property on this class.
		 * 
		 * <p>
		 * the map is keyed by the name of the attribute and the value is the
		 * string value of the attribute.
		 * 
		 * the map returned by this method is live, and you can add new
		 * attribute by updating the map directly. Because of this design,
		 * there's no setter.
		 * 
		 * 
		 * @return always non-null
		 */
		public Map<QName, String> getOtherAttributes() {
			return otherAttributes;
		}

		/**
		 * <p>
		 * Java class for anonymous complex type.
		 * 
		 * <p>
		 * The following schema fragment specifies the expected content
		 * contained within this class.
		 * 
		 * <pre>
		 * &lt;complexType&gt;
		 *   &lt;complexContent&gt;
		 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
		 *       &lt;attribute name="name" use="required" type="{http://lca.jrc.it/ILCD/Common}MethodOfReviewValue" /&gt;
		 *       &lt;anyAttribute processContents='lax' namespace='##other'/&gt;
		 *     &lt;/restriction&gt;
		 *   &lt;/complexContent&gt;
		 * &lt;/complexType&gt;
		 * </pre>
		 * 
		 * 
		 */
		@XmlAccessorType(XmlAccessType.FIELD)
		public static class Method {

			@XmlAttribute(required = true)
			protected MethodOfReviewValue name;

			@XmlAnyAttribute
			private Map<QName, String> otherAttributes = new HashMap<QName, String>();

			/*
			 * (non-Javadoc)
			 * 
			 * @see de.fzk.iai.ilcd.api.vo.types.process.IMethod#getName()
			 */
			public MethodOfReviewValue getName() {
				return name;
			}

			/**
			 * Sets the value of the name property.
			 * 
			 * @param value
			 *            allowed object is {@link MethodOfReviewValue }
			 * 
			 */
			public void setName(MethodOfReviewValue value) {
				this.name = value;
			}

			/**
			 * Gets a map that contains attributes that aren't bound to any
			 * typed property on this class.
			 * 
			 * <p>
			 * the map is keyed by the name of the attribute and the value is
			 * the string value of the attribute.
			 * 
			 * the map returned by this method is live, and you can add new
			 * attribute by updating the map directly. Because of this design,
			 * there's no setter.
			 * 
			 * 
			 * @return always non-null
			 */
			public Map<QName, String> getOtherAttributes() {
				return otherAttributes;
			}

		}

	}

}
