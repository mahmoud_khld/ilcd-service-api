/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.client.impl.vo.dataset;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import de.fzk.iai.ilcd.service.client.impl.vo.types.lciamethod.TimeInformationType;
import de.fzk.iai.ilcd.service.model.ILCIAMethodVO;
import de.fzk.iai.ilcd.service.model.enums.AreaOfProtectionValue;
import de.fzk.iai.ilcd.service.model.enums.LCIAImpactCategoryValue;
import de.fzk.iai.ilcd.service.model.enums.TypeOfLCIAMethodValue;

@XmlRootElement(name = "LCIAMethod", namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/LCIAMethod")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "type", "methodology", "impactCategory", "areaOfProtection", "impactIndicator", "timeInformation" })
public class LCIAMethodDataSetVO extends DataSetVO implements ILCIAMethodVO {

	public LCIAMethodDataSetVO() {
	}

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/LCIAMethod")
	protected TypeOfLCIAMethodValue type;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/LCIAMethod")
	protected List<String> methodology;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/LCIAMethod")
	protected List<LCIAImpactCategoryValue> impactCategory;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/LCIAMethod")
	protected List<AreaOfProtectionValue> areaOfProtection;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/LCIAMethod")
	protected String impactIndicator;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/LCIAMethod", name = "time")
	protected TimeInformationType timeInformation;

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.ILCIAMethodVO#getType()
	 */
	public TypeOfLCIAMethodValue getType() {
		return type;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fzk.iai.ilcd.api.vo.dataset.ILCIAMethodVO#setType(de.fzk.iai.ilcd
	 * .api.vo.types.lciamethod.TypeOfLCIAMethodValue)
	 */
	public void setType(TypeOfLCIAMethodValue type) {
		this.type = type;
	}

	/**
	 * @return the methodologies
	 */
	public List<String> getMethodology() {
		if (methodology == null)
			methodology = new ArrayList<String>();
		return methodology;
	}

	/**
	 * @return the impactCategories
	 */
	public List<LCIAImpactCategoryValue> getImpactCategory() {
		if (impactCategory == null)
			impactCategory = new ArrayList<LCIAImpactCategoryValue>();
		return impactCategory;
	}

	/**
	 * @return the areas of protection
	 */
	public List<AreaOfProtectionValue> getAreaOfProtection() {
		if (areaOfProtection == null)
			areaOfProtection = new ArrayList<AreaOfProtectionValue>();
		return areaOfProtection;
	}

	/**
	 * @return the impactIndicator
	 */
	public String getImpactIndicator() {
		return impactIndicator;
	}

	/**
	 * @param impactIndicator
	 *            the impactIndicator to set
	 */
	public void setImpactIndicator(String impactIndicator) {
		this.impactIndicator = impactIndicator;
	}

	/**
	 * @return the timeInformation
	 */
	public TimeInformationType getTimeInformation() {
		return timeInformation;
	}

	/**
	 * @param timeInformation
	 *            the timeInformation to set
	 */
	public void setTimeInformation(TimeInformationType timeInformation) {
		this.timeInformation = timeInformation;
	}

	// public IGlobalReference getReferenceQuantity() {
	// // TODO Auto-generated method stub
	// return null;
	// }

}
