/**
 * 
 */
/**
 * @author oliver.kusche
 * 
 */
@javax.xml.bind.annotation.XmlSchema(namespace = "http://www.iai.kit.edu/EPD/2013", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package de.fzk.iai.ilcd.service.client.impl.vo.epd;