/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.client.impl.vo.nodeinfo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import de.fzk.iai.ilcd.service.client.impl.vo.types.common.LString;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.MultiLangString;
import de.fzk.iai.ilcd.service.model.INodeInfo;
import de.fzk.iai.ilcd.service.model.common.IMultiLangString;

@XmlRootElement(name = "nodeInfo", namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/NodeInfo")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "nodeID", "name", "operator", "description", "baseURL", "administrativeContact" })
public class NodeInfo implements INodeInfo {

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/NodeInfo")
	protected String nodeID;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/NodeInfo")
	protected String name;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/NodeInfo")
	protected String operator;

	@XmlElement(type = LString.class, namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/NodeInfo")
	protected List<LString> description;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/NodeInfo")
	protected String baseURL;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/NodeInfo")
	protected AdministrativeContact administrativeContact;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public IMultiLangString getDescription() {
		if (description == null)
			description = new ArrayList<LString>();
		return new MultiLangString(description);
	}

	/**
	 * @return the baseURL
	 */
	public String getBaseURL() {
		return baseURL;
	}

	/**
	 * @param baseURL
	 *            the baseURL to set
	 */
	public void setBaseURL(String baseURL) {
		this.baseURL = baseURL;
	}

	/**
	 * @return the operator
	 */
	public String getOperator() {
		return operator;
	}

	/**
	 * @param operator
	 *            the operator to set
	 */
	public void setOperator(String operator) {
		this.operator = operator;
	}

	/**
	 * @return the nodeID
	 */
	public String getNodeID() {
		return nodeID;
	}

	/**
	 * @param nodeID
	 *            the nodeID to set
	 */
	public void setNodeID(String nodeID) {
		this.nodeID = nodeID;
	}

	/**
	 * @return the adminName
	 */
	public String getAdminName() {
		if (this.administrativeContact != null)
			return this.administrativeContact.getAdminName();
		else
			return null;
	}

	/**
	 * @param adminName
	 *            the adminName to set
	 */
	public void setAdminName(String adminName) {
		if (adminName == null)
			return;
		if (this.administrativeContact == null)
			this.administrativeContact = new AdministrativeContact();
		this.administrativeContact.setAdminName(adminName);
	}

	/**
	 * @return the adminEMail
	 */
	public String getAdminEMail() {
		if (this.administrativeContact != null)
			return this.administrativeContact.getAdminEMail();
		else
			return null;
	}

	/**
	 * @param adminEMail
	 *            the adminEMail to set
	 */
	public void setAdminEMail(String adminEMail) {
		if (adminEMail == null)
			return;
		if (this.administrativeContact == null)
			this.administrativeContact = new AdministrativeContact();
		this.administrativeContact.setAdminEMail(adminEMail);
	}

	/**
	 * @return the adminPhone
	 */
	public String getAdminPhone() {
		if (this.administrativeContact != null)
			return this.administrativeContact.getAdminPhone();
		else
			return null;
	}

	/**
	 * @param adminPhone
	 *            the adminPhone to set
	 */
	public void setAdminPhone(String adminPhone) {
		if (adminPhone == null)
			return;
		if (this.administrativeContact == null)
			this.administrativeContact = new AdministrativeContact();
		this.administrativeContact.setAdminPhone(adminPhone);
	}

	/**
	 * @return the adminWWW
	 */
	public String getAdminWWW() {
		if (this.administrativeContact != null)
			return this.administrativeContact.getAdminWWW();
		else
			return null;
	}

	/**
	 * @param adminWWW
	 *            the adminWWW to set
	 */
	public void setAdminWWW(String adminWWW) {
		if (adminWWW == null)
			return;
		if (this.administrativeContact == null)
			this.administrativeContact = new AdministrativeContact();
		this.administrativeContact.setAdminWWW(adminWWW);
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(List<LString> description) {
		this.description = description;
	}

	public void setDescription(String lang, String name) {
		LString lString = new LString(lang, name);
		addDescription(lString);
	}

	public void setDescription(String name) {
		LString lString = new LString(name);
		addDescription(lString);
	}

	public void addDescription(LString lName) {
		if (this.description == null)
			this.description = new ArrayList<LString>();
		lName.insertWithoutDuplicates(this.description);
	}

	@XmlType(propOrder = { "adminName", "adminEMail", "adminPhone", "adminWWW" })
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class AdministrativeContact {
		@XmlElement(name = "centralContactPoint", namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Contact")
		protected String adminName;

		@XmlElement(name = "email", namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Contact")
		protected String adminEMail;

		@XmlElement(name = "phone", namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Contact")
		protected String adminPhone;

		@XmlElement(name = "www", namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI/Contact")
		protected String adminWWW;

		/**
		 * @return the adminName
		 */
		public String getAdminName() {
			return adminName;
		}

		/**
		 * @param adminName
		 *            the adminName to set
		 */
		public void setAdminName(String adminName) {
			this.adminName = adminName;
		}

		/**
		 * @return the adminEMail
		 */
		public String getAdminEMail() {
			return adminEMail;
		}

		/**
		 * @param adminEMail
		 *            the adminEMail to set
		 */
		public void setAdminEMail(String adminEMail) {
			this.adminEMail = adminEMail;
		}

		/**
		 * @return the adminPhone
		 */
		public String getAdminPhone() {
			return adminPhone;
		}

		/**
		 * @param adminPhone
		 *            the adminPhone to set
		 */
		public void setAdminPhone(String adminPhone) {
			this.adminPhone = adminPhone;
		}

		/**
		 * @return the adminWWW
		 */
		public String getAdminWWW() {
			return adminWWW;
		}

		/**
		 * @param adminWWW
		 *            the adminWWW to set
		 */
		public void setAdminWWW(String adminWWW) {
			this.adminWWW = adminWWW;
		}

	}
}
