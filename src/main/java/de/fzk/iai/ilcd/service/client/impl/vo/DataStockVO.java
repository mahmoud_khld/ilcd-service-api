/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/

package de.fzk.iai.ilcd.service.client.impl.vo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import de.fzk.iai.ilcd.service.client.impl.vo.types.common.LString;
import de.fzk.iai.ilcd.service.client.impl.vo.types.common.MultiLangString;
import de.fzk.iai.ilcd.service.model.IDataStockVO;
import de.fzk.iai.ilcd.service.model.common.IMultiLangString;

@XmlRootElement(name = "dataStock", namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "uuid", "shortName", "name", "description", "roles" })
public class DataStockVO implements IDataStockVO {

	@XmlAttribute(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI")
	protected boolean root;

	@XmlElement(name = "uuid")
	protected String uuid;

	@XmlElement(namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI")
	protected String shortName;

	@XmlElement(type = LString.class, namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI")
	protected List<LString> name;

	@XmlElement(type = LString.class, namespace = "http://www.ilcd-network.org/ILCD/ServiceAPI")
	protected List<LString> description;

	@XmlElement(name = "role")
	protected List<String> roles;
	
	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	/**
	 * @return the name
	 */
	public IMultiLangString getName() {
		if (name == null)
			name = new ArrayList<LString>();
		return new MultiLangString(name);
	}

	/**
	 * @return the description
	 */
	public IMultiLangString getDescription() {
		if (description == null)
			description = new ArrayList<LString>();
		return new MultiLangString(description);
	}

	/**
	 * @return the shortName
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * @param shortName
	 *            the shortName to set
	 */
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(List<LString> name) {
		this.name = name;
	}

	public void setName(String lang, String name) {
		LString lString = new LString(lang, name);
		addName(lString);
	}

	public void setName(String name) {
		LString lString = new LString(name);
		addName(lString);
	}

	public void addName(LString lName) {
		if (this.name == null)
			this.name = new ArrayList<LString>();
		lName.insertWithoutDuplicates(this.name);
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(List<LString> description) {
		this.description = description;
	}

	public void setDescription(String lang, String description) {
		LString lString = new LString(lang, description);
		addDescription(lString);
	}

	public void setDescription(String description) {
		LString lString = new LString(description);
		addDescription(lString);
	}

	public void addDescription(LString lName) {
		if (this.description == null)
			this.description = new ArrayList<LString>();
		lName.insertWithoutDuplicates(this.description);
	}

	public boolean isRoot() {
		return root;
	}

	public void setRoot(boolean root) {
		this.root = root;
	}

	
	public List<String> getRoles() {
		if (roles==null)
			roles = new ArrayList<String>();
		return roles;
	}

	
	public void setRoles( List<String> roles ) {
		this.roles = roles;
	}
}
