/*******************************************************************************
 * Copyright (c) 2011 Karlsruhe Institute of Technology (KIT) - Institute for 
 * Applied Computer Science (IAI). 
 * 
 * This file is part of the Java Service API for ILCD.
 * 
 * Java Service API for ILCD is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 * 
 * The Java Service API for ILCD is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file.  If not, see &lt;http://www.gnu.org/licenses/&gt;.
 ******************************************************************************/


package de.fzk.iai.ilcd.service.model.process;

import de.fzk.iai.ilcd.service.model.enums.DataQualityIndicatorName;
import de.fzk.iai.ilcd.service.model.enums.QualityValue;

// TODO: Auto-generated Javadoc
/**
 * The Interface IDataQualityIndicator.
 */
public interface IDataQualityIndicator {

	/**
	 * Gets the value of the name property.
	 * 
	 * @return possible object is {@link DataQualityIndicatorName }
	 * 
	 */
	public abstract DataQualityIndicatorName getName();

	/**
	 * Gets the value of the value property.
	 * 
	 * @return possible object is {@link QualityValue }
	 * 
	 */
	public abstract QualityValue getValue();

	/**
	 * Gets the value of the numericValue property.
	 * 
	 * @return possible object is {@link QualityValue }
	 * 
	 */
	public abstract Double getNumericValue();
}
