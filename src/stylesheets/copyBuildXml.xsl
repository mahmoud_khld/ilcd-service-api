<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:internal="http://iai.fzk.de/internal" exclude-result-prefixes="internal">

  <xsl:output indent="yes" method="xml"/>

  <xsl:template match="processing-instruction()|comment()">
    <xsl:copy/>
  </xsl:template>

  <xsl:template match="/">
    <xsl:apply-templates select="@*|node()"/>
  </xsl:template>

  <!-- do not copy -->
  <xsl:template match="import"/>

  <!-- copy -->
  <xsl:template match="*">
    <xsl:if test="not(@internal:internal)">
      <xsl:element name="{name()}" namespace="{namespace-uri()}">
        <!-- go process attributes and children -->
        <xsl:apply-templates select="@*|node()"/>
      </xsl:element>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="@*">
    <xsl:copy-of select="."/>
  </xsl:template>

</xsl:stylesheet>
