SODA_HOST=$1

STATUS_CODE=$(curl -s -o /dev/null -w "%{http_code}" --max-time 1 $SODA_HOST)
while [[ "$STATUS_CODE" != "200" ]]
do
    if [[ "$STATUS_CODE" == "000" ]]
    then
      STATUS_CODE="Timeout"
    fi
    echo "Waiting for server '$SODA_HOST' ($STATUS_CODE) ..."
    sleep 1
    STATUS_CODE=$(curl -s -o /dev/null -w "%{http_code}" --max-time 1 $SODA_HOST)
done
echo $STATUS_CODE